﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="InformesFHC._Default" %>

<%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=12.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" namespace="Microsoft.Reporting.WebForms" tagprefix="rsweb" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="jumbotron">
        <h1>ASP.NET</h1>
        <p class="lead">ASP.NET is a free web framework for building great Web sites and Web applications using HTML, CSS, and JavaScript.</p>
        <p><a href="http://www.asp.net" class="btn btn-primary btn-lg">Learn more &raquo;</a></p>
    </div>

    <div class="row">
        <div class="col-md-4">
            <h2>Getting started</h2>
            <p>
            </p>
            <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" TypeName="InformesFHC.DataSetTableAdapters.FHDC_FUN_ENC_CONS_HIST_VSTableAdapter"></asp:ObjectDataSource>
            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataSourceID="ObjectDataSource1">
                <Columns>
                    <asp:BoundField DataField="RUT_ASESOR" HeaderText="RUT_ASESOR" SortExpression="RUT_ASESOR" />
                    <asp:BoundField DataField="NOMBRE_ASESOR" HeaderText="NOMBRE_ASESOR" SortExpression="NOMBRE_ASESOR" />
                    <asp:BoundField DataField="RUT_CONTRATANTE_FMT" HeaderText="RUT_CONTRATANTE_FMT" SortExpression="RUT_CONTRATANTE_FMT" />
                    <asp:BoundField DataField="NOM_CONTRATANTE" HeaderText="NOM_CONTRATANTE" SortExpression="NOM_CONTRATANTE" />
                    <asp:BoundField DataField="RUT_FALLECIDO_FMT" HeaderText="RUT_FALLECIDO_FMT" SortExpression="RUT_FALLECIDO_FMT" />
                    <asp:BoundField DataField="NOM_FALLECIDO" HeaderText="NOM_FALLECIDO" SortExpression="NOM_FALLECIDO" />
                    <asp:BoundField DataField="UNEG_EMPR_DESC" HeaderText="UNEG_EMPR_DESC" SortExpression="UNEG_EMPR_DESC" />
                    <asp:BoundField DataField="SUCU_DESC" HeaderText="SUCU_DESC" SortExpression="SUCU_DESC" />
                    <asp:BoundField DataField="OPER_NUM" HeaderText="OPER_NUM" SortExpression="OPER_NUM" />
                    <asp:BoundField DataField="OPER_NUM_FMT" HeaderText="OPER_NUM_FMT" SortExpression="OPER_NUM_FMT" />
                    <asp:BoundField DataField="OPER_NECE_TIPO" HeaderText="OPER_NECE_TIPO" SortExpression="OPER_NECE_TIPO" />
                    <asp:BoundField DataField="OPER_FECH" HeaderText="OPER_FECH" SortExpression="OPER_FECH" />
                    <asp:BoundField DataField="OPER_ESTA_DESC" HeaderText="OPER_ESTA_DESC" SortExpression="OPER_ESTA_DESC" />
                    <asp:BoundField DataField="PLAN_COD" HeaderText="PLAN_COD" SortExpression="PLAN_COD" />
                    <asp:BoundField DataField="PLAN_DESC" HeaderText="PLAN_DESC" SortExpression="PLAN_DESC" />
                    <asp:BoundField DataField="PRSE_COD" HeaderText="PRSE_COD" SortExpression="PRSE_COD" />
                    <asp:BoundField DataField="PRSE_DESC" HeaderText="PRSE_DESC" SortExpression="PRSE_DESC" />
                    <asp:BoundField DataField="PRECIO_PROD" HeaderText="PRECIO_PROD" SortExpression="PRECIO_PROD" />
                    <asp:BoundField DataField="NUM_REQU_FMT" HeaderText="NUM_REQU_FMT" SortExpression="NUM_REQU_FMT" />
                    <asp:BoundField DataField="REQU_TIPO_DESC" HeaderText="REQU_TIPO_DESC" SortExpression="REQU_TIPO_DESC" />
                    <asp:BoundField DataField="FECH_REQU" HeaderText="FECH_REQU" SortExpression="FECH_REQU" />
                    <asp:BoundField DataField="REQU_ESTA_DESC" HeaderText="REQU_ESTA_DESC" SortExpression="REQU_ESTA_DESC" />
                    <asp:BoundField DataField="MOVI_TIPO" HeaderText="MOVI_TIPO" SortExpression="MOVI_TIPO" />
                    <asp:BoundField DataField="MOVI_FECH_PRG_EJEC" HeaderText="MOVI_FECH_PRG_EJEC" SortExpression="MOVI_FECH_PRG_EJEC" />
                    <asp:BoundField DataField="MOVI_ESTA_COD" HeaderText="MOVI_ESTA_COD" SortExpression="MOVI_ESTA_COD" />
                    <asp:BoundField DataField="REQU_SEC" HeaderText="REQU_SEC" SortExpression="REQU_SEC" />
                    <asp:BoundField DataField="MOVI_SEC" HeaderText="MOVI_SEC" SortExpression="MOVI_SEC" />
                    <asp:BoundField DataField="RQSP_SEC" HeaderText="RQSP_SEC" SortExpression="RQSP_SEC" />
                </Columns>
            </asp:GridView>
            <p>
                <a class="btn btn-default" href="http://go.microsoft.com/fwlink/?LinkId=301948">Learn more &raquo;</a>
            </p>
        </div>
        <div class="col-md-4">
            <h2>Get more libraries</h2>
            <p>
                NuGet is a free Visual Studio extension that makes it easy to add, remove, and update libraries and tools in Visual Studio projects.
            </p>
            <p>
                <a class="btn btn-default" href="http://go.microsoft.com/fwlink/?LinkId=301949">Learn more &raquo;</a>
            </p>
        </div>
        <div class="col-md-4">
            <h2>Web Hosting</h2>
            <p>
                You can easily find a web hosting company that offers the right mix of features and price for your applications.
            </p>
            <p>
                <a class="btn btn-default" href="http://go.microsoft.com/fwlink/?LinkId=301950">Learn more &raquo;</a>
            </p>
        </div>
    </div>

</asp:Content>
