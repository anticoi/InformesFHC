﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(InformesFHC.Startup))]
namespace InformesFHC
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
